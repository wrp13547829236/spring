package com.wrp.service.impl;

import com.wrp.dao.UserDao;
import com.wrp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;

//@Component("userService")
@Service("userService")
public class UserServiceImpl implements UserService {

    @Value("${jdbc.driverClassName}")
    private String name;


//    @Autowired// 当只有一个实现类时，只需要这个注解就能注入
//    @Qualifier("userDao")
    @Resource(name = "userDao")
    private UserDao userDao;

//    public void setUserDao(UserDao userDao) {
//        this.userDao = userDao;
//    }

    public void save() {
        System.out.println("service do it......");
        userDao.save();
        System.out.println(name);
    }

    @PostConstruct
    public void init(){
        System.out.println("初始化了。。。");
    }

    @PreDestroy
    public void destroy(){
        System.out.println("销毁了。。。");
    }
}
