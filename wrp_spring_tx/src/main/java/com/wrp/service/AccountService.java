package com.wrp.service;

import com.wrp.domain.Account;

public interface AccountService {

    /**
     * 转账
     * @param account_resource
     * @param account_target
     * @param money
     */
    public void transfer(Account account_resource, Account account_target, float money);

    public Account queryByName(String name);
}
