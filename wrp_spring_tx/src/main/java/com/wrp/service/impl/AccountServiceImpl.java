package com.wrp.service.impl;

import com.wrp.dao.AccountDao;
import com.wrp.domain.Account;
import com.wrp.service.AccountService;

public class AccountServiceImpl implements AccountService {
    private AccountDao accountDao;

    public void setAccountDao(AccountDao accountDao) {
        this.accountDao = accountDao;
    }

    @Override
    public void transfer(Account account_resource, Account account_target, float money) {
        accountDao.addMoney(account_target, money);
        int i = 1 / 0;
        accountDao.reduce(account_resource, money);
    }

    @Override
    public Account queryByName(String name) {
        return accountDao.queryByName(name);
    }
}
