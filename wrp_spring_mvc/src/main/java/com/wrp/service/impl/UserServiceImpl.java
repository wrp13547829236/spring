package com.wrp.service.impl;

import com.wrp.dao.UserDao;
import com.wrp.service.UserService;

public class UserServiceImpl implements UserService {

    private UserDao userDao;

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void save() {
        System.out.println("UserService.....");
        userDao.save();
    }
}
