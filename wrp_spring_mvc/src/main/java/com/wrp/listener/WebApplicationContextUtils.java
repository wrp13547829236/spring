package com.wrp.listener;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.servlet.ServletContext;

/**
 * ApplicationContext工具集
 */
public class WebApplicationContextUtils {
    private static final String applicationContextName = "app";

    /**
     * 创建spring上下文
     * @param servletContext
     */
    public static void createApplicationContext(ServletContext servletContext){
        // 读取web.xml文件中的全局初始化参数
        String contextConfigLocation = servletContext.getInitParameter("contextConfigLocation");
        ApplicationContext app = new ClassPathXmlApplicationContext(contextConfigLocation);
        // 将context 存储到ServletContext域中
        servletContext.setAttribute(applicationContextName, app);
        System.out.println("Spring容器创建成功！");
    }

    /**
     * 从servletContext对象中获取spring上下文
     * @param servletContext
     * @return
     */
    public static ApplicationContext getApplicationContext(ServletContext servletContext){
        return (ApplicationContext) servletContext.getAttribute(applicationContextName);
    }
}
