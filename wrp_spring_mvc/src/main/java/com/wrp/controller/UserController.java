package com.wrp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wrp.po.User;
import com.wrp.po.Vo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {

    /**
     * 页面跳转方式1：返回视字符串
     * @return
     */
    // 请求路径：http://localhost:8080/user/quick
    @RequestMapping("/quick")// 可以用于类或方法上
    public String save(){
        System.out.println("Controller.....");
        //
//        return "user.jsp";// 访问user/user.jsp 相对于当前资源所在的位置http://localhost:8080/user
//        return "/user.jsp";// 访问http://localhost:8080/user.jsp 相对于web系统的位置http://localhost:8080
//        return "redirect:/jsp/user.jsp";
        return "user";// spring-mvc.xml文件中配置了视图解析器前缀和后缀, redirect:无法访问WEB-INF（受保护文件夹）
    }

    /**
     * 页面跳转方式2：返回模型视图对象
     * @return
     */
    @RequestMapping("/quick2")
    public ModelAndView save2(){
        /**
         * Model: 模型数据
         *
         * View：视图
         */
        ModelAndView modelAndView = new ModelAndView();
        // 设置模型数据
        modelAndView.addObject("username", "wrp");
        // 设置视图
        modelAndView.setViewName("user");// 返回/jsp/user.jsp
        return modelAndView;
    }

    /**
     * 回写数据方式2：直接返回字符串
     * 框架根据形参注入
     *  举一反三：还有HttpServletRequest、ModelAndView、Model等等都行
     * @return
     */
    @RequestMapping("/quick3")
    public void save3(HttpServletResponse response) throws IOException {
        response.getWriter().write("hellp");
    }

    @RequestMapping("/quick4")
    @ResponseBody// 标识该返回的字符串是响应体
    public String save4() throws IOException {
        return "{\"username\":\"zs\"}";// 返回json格式字符串
    }


    @RequestMapping("/quick5")
    @ResponseBody// 标识该返回的字符串是响应体
    public String save5() throws IOException {
        User user = new User();
        user.setName("zhangsan");
        user.setAge(18);
        // 使用json工具将对象解析为json字符串
        ObjectMapper om = new ObjectMapper();
        return om.writeValueAsString(user);
    }

    /**
     * 回写数据方式2：返回对象或集合
     * @return
     * @throws IOException
     *
     * 需要配置处理器适配器，让其将对象帮我们转换为json字符串
     */
    @RequestMapping("/quick6")
    @ResponseBody// 标识该返回的字符串是响应体
    // http://localhost:8081/user/quick6?name=zhangsan&age=22
    // 请求参数与方法的参数名一致，自动映射
    public User save6(String name, int age) {
        User user = new User();
        user.setName(name);
        user.setAge(age);

        return user;
    }

    @RequestMapping("/quick7")
    @ResponseBody// 标识该返回的字符串是响应体
    // http://localhost:8081/user/quick7?name=%E5%AD%9F%E5%BE%B7&age=22
    // 请求参数与方法的POJO参数的属性名一致，自动映射
    public User save7(User user) {
        return user;
    }

    @RequestMapping("/quick8")
    @ResponseBody// 标识该返回的字符串是响应体
    // http://localhost:8081/user/quick8?strs=123&strs=123&strs=123
    // 请求参数与Controller中的业务方法的参数数组名一致，自动映射匹配
    public String save8(String strs) {
        String str = "[";
        str = str + strs.concat(",") + "]";
        return str;
    }

    @RequestMapping("/quick9")
    @ResponseBody// 标识该返回的字符串是响应体
    // http://localhost:8081/user/quick8?strs=123&strs=123&strs=123
    // 需要将集合封装到VO对象中，采用post方式提交
    public void save9(Vo vo) {

        System.out.println(vo);
    }

    @RequestMapping("/quick10")
    @ResponseBody// 标识该返回的字符串是响应体
    public void save10(@RequestBody List<User> userList) {

        System.out.println(userList);
    }

    @RequestMapping("/quick11")
    @ResponseBody// 标识该返回的字符串是响应体
    // @RequestParam 用于请求参数绑定
    public void save11(@RequestParam(value = "name", defaultValue = "nihao") String hh){
        System.out.println(hh);
    }

    @RequestMapping("/quick12/{name}")
    @ResponseBody// 标识该返回的字符串是响应体
    // @PathVariable 用于解析占位符{name}
    public void save12(@PathVariable(value = "name") String hh){
        System.out.println(hh);
    }

    @RequestMapping("/quick13")
    @ResponseBody// 标识该返回的字符串是响应体
    public void save13(@RequestParam("date") Date date){
        System.out.println(date);
    }

    @RequestMapping("/quick14")
    @ResponseBody// 标识该返回的字符串是响应体
    public void save14(@RequestParam("date") Date date){
        System.out.println(date);
    }

    @RequestMapping("/quick15")
    @ResponseBody// 标识该返回的字符串是响应体
    // @RequestHeader 获取请求头的键值对
    public void save15(@RequestHeader(value = "User-Agent") String header){
        System.out.println(header);
    }

    @RequestMapping("/quick16")
    @ResponseBody// 标识该返回的字符串是响应体
    // @CookieValue 获取请求头的cookie
    public void save16(@CookieValue(value = "JSESSIONID") String header){
        System.out.println(header);
    }
//    文件上传
    @RequestMapping("/quick17")
    @ResponseBody// 标识该返回的字符串是响应体
    // 方法参数如果和请求参数名不一致，可以使用@RequestParam进行参数绑定
    public void save17(@RequestParam("username") String name, @RequestParam("uploadFile")MultipartFile upload) throws IOException {
        System.out.println(name);
//        System.out.println(upload);
        // 获取文件名
        String filename = upload.getOriginalFilename();
        upload.transferTo(new File("E:\\Temp\\" + filename));
    }
}
