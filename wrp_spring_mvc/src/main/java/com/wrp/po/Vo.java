package com.wrp.po;

import java.util.List;

public class Vo {
    List<User> users;

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    @Override
    public String toString() {
        return "Vo{" +
                "users=" + users +
                '}';
    }
}
