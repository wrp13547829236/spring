package com.wrp.first;

import com.wrp.domain.Student;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class MybatisTest {
    static SqlSessionFactory builder;
    static{
        // 获取核心配置文件
        InputStream resourceAsStream = null;
        try {
            resourceAsStream = Resources.getResourceAsStream("SqlMapConfig.xml");
        } catch (IOException e) {
            e.printStackTrace();
        }
        // 获取Session工厂
        builder = new SqlSessionFactoryBuilder().build(resourceAsStream);
    }

    @Test
    public void findAllTest() throws IOException {
        // 获取核心配置文件
        InputStream resourceAsStream = Resources.getResourceAsStream("SqlMapConfig.xml");
        // 获取Session工厂
        SqlSessionFactory build = new SqlSessionFactoryBuilder().build(resourceAsStream);
        // 获取Session
        SqlSession sqlSession = build.openSession();
        // 执行操作
        List<Student> stu = sqlSession.selectList("studentMapper.findAll");
        // 输出操作
        System.out.println(stu);
        // 关闭资源
        sqlSession.close();
    }

    @Test
    public void addTest(){
        SqlSession sqlSession = builder.openSession();
        Student stu = new Student();
        stu.setName("王瑞平");
        stu.setAge(18);
        sqlSession.insert("studentMapper.add", stu);
        // 提交事务
        sqlSession.commit();
        sqlSession.close();
    }

    @Test
    public void updateTest(){
        SqlSession sqlSession = builder.openSession();
        Student stu = new Student();
        stu.setName("王瑞平");
        stu.setAge(22);
        stu.setId(6);
        sqlSession.update("studentMapper.update", stu);
        // 提交事务
        sqlSession.commit();
        sqlSession.close();
    }

    @Test
    public void deleteTest(){
        SqlSession sqlSession = builder.openSession();
        sqlSession.delete("studentMapper.delete", 1);
        // 提交事务
        sqlSession.commit();
        sqlSession.close();
    }
}
