package com.wrp.mapper;

import com.wrp.domain.Course;

import java.util.List;

public interface CourseMapper {
    public List<Course> findAll();
}
