package com.wrp.mapper;

import com.wrp.domain.Student;

import java.util.List;

public interface StudentMapper {

    public void insert(Student student);

    public void delete(int id);

    public void update(Student student);

    public Student findById(int id);

    public List<Student> findAll();
}
