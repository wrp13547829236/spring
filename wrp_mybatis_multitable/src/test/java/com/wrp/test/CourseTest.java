package com.wrp.test;

import com.wrp.domain.Course;
import com.wrp.domain.Student;
import com.wrp.mapper.CourseMapper;
import com.wrp.mapper.StudentMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class CourseTest {

    @Test
    public void findAllTest() throws IOException {
        InputStream resourceAsStream = Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactory build = new SqlSessionFactoryBuilder().build(resourceAsStream);
        SqlSession sqlSession = build.openSession(true);

        CourseMapper mapper = sqlSession.getMapper(CourseMapper.class);
        List<Course> courseList = mapper.findAll();

        for(Course course: courseList){
            System.out.println(course);
        }
    }
}
