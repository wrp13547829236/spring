package com.wrp.factory;

import com.wrp.dao.UserDao;

public class UserDaoFactory {
    // 工厂静态方法获取bean
    public static UserDao getUserDao(){
        return new UserDao() {
            public void save() {
                System.out.println("UserDao.....");
            }
        };
    }
    // 工厂实例方法获取bean
    public UserDao getUserDaoByInst(){
        return new UserDao() {
            public void save() {
                System.out.println("UserDao.....");
            }
        };
    }
}
