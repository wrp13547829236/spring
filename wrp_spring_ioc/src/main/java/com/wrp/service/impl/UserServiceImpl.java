package com.wrp.service.impl;

import com.wrp.dao.UserDao;
import com.wrp.domain.User;
import com.wrp.service.UserService;

import java.util.List;
import java.util.Map;
import java.util.Properties;

public class UserServiceImpl implements UserService {

    private UserDao userDao;
    private List<User> userList;
    private Map<String, String> strMap;
    private Properties properties;

    public UserServiceImpl() {
    }

    public UserServiceImpl(UserDao userDao, List<User> userList) {
        this.userDao = userDao;
        this.userList = userList;
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }

    public void setStrMap(Map<String, String> strMap) {
        this.strMap = strMap;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }

    public void save() {
        System.out.println("业务层调用了dao");
        userDao.save();
    }

    @Override
    public String toString() {
        return "UserServiceImpl{" +
                "userDao=" + userDao +
                ", userList=" + userList +
                ", strMap=" + strMap +
                ", properties=" + properties +
                '}';
    }
}
