package com.wrp.dao.impl;

import com.wrp.dao.UserDao;

public class UserDaoImpl implements UserDao {

    public UserDaoImpl() {
        System.out.println("UserDao创建了....");
    }

    public void init(){
        System.out.println("UserDao初始化了......");
    }

    public void destroy(){
        System.out.println("UserDao销毁了......");
    }

    public void save() {
        System.out.println("save.....");
    }
}
