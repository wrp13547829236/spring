package com.wrp.client;

import com.wrp.dao.UserDao;
import com.wrp.dao.impl.UserDaoImpl;
import com.wrp.dao.impl.UserDaoImpl2;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Client {
    public static void main(String[] args) {
        ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserDao ud = ac.getBean(UserDao.class);// 参数可用接口或者配置中的class子类
        ud.save();
//        UserDao ud1 = (UserDao) ac.getBean("userDao");
//        ud1.save();

    }
}
