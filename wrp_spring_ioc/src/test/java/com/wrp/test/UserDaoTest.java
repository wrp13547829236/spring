package com.wrp.test;


import com.wrp.dao.UserDao;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class UserDaoTest {

    @Test
    public void saveTest(){
        ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
        UserDao ud = (UserDao) ac.getBean("userDao");
        ud.save();
        // 手动关闭容器，测试销毁方法
        // ((ClassPathXmlApplicationContext)ac).close();
    }
}
