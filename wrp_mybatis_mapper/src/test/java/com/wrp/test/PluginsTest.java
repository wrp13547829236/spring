package com.wrp.test;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wrp.domain.Student;
import com.wrp.mapper.StudentMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class PluginsTest {

    @Test
    public void test() throws IOException {
        InputStream inputStream = Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactory builder = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = builder.openSession(true);
        StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);

        // 设置分页参数 当前页，每页条数
        PageHelper.startPage(2, 3);

        List<Student> s = mapper.findAll();
        for(Student stu: s){
            System.out.println(stu);
        }

        PageInfo<Student> info = new PageInfo<Student>(s);
        System.out.println("当前页：" + info.getPageNum());
        System.out.println("每页显示条数：" + info.getPageSize());
        System.out.println("总条数：" + info.getTotal());
        System.out.println("总页数：" + info.getPages());
        System.out.println("上一页：" + info.getPrePage());
        System.out.println("下一页：" + info.getNextPage());
        System.out.println("是否是第一页：" + info.isIsFirstPage());
        System.out.println("是否是最后一页：" + info.isIsLastPage());

    }
}
