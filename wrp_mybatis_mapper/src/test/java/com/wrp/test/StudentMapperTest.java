package com.wrp.test;

import com.wrp.domain.Student;
import com.wrp.mapper.StudentMapper;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class StudentMapperTest {

    @Test
    public void convertTest() throws IOException {
        InputStream inputStream = Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactory builder = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = builder.openSession(true);
        StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);

        Student stu = new Student();
        stu.setName("nic");
        stu.setAge(22);
        stu.setBirthday(new Date());

        mapper.add(stu);
    }

    @Test
    // 测试JDBC类型能否转Java类型
    public void convertTest2() throws IOException {
        InputStream inputStream = Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactory builder = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = builder.openSession(true);
        StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);


        Student s = mapper.findById(8);
        System.out.println(s);
    }

    @Test
    public void findByConditionTest() throws IOException {
        InputStream inputStream = Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactory builder = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = builder.openSession(true);
        StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);

        Student stu = new Student();
//        stu.setId(7);
//        stu.setName("nic");
        stu.setAge(22);

        List<Student> res = mapper.findByCondition(stu);
        System.out.println(res);
    }


    @Test
    public void findByIdsTest() throws IOException {
        InputStream inputStream = Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactory builder = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = builder.openSession(true);
        StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);

        List<Integer> ids = new ArrayList<Integer>();
        for (int i = 0 ; i < 7; i++){
            ids.add(i);
        }

        List<Student> res = mapper.findByIds(ids);
        System.out.println(res);
    }
}
