package com.wrp.mapper;

import com.wrp.domain.Student;

import java.util.List;

public interface StudentMapper {

    List<Student> findByCondition(Student stu);

    List<Student> findByIds(List<Integer> ids);

    Student findById(int id);

    void add(Student student);

    List<Student> findAll();
}
