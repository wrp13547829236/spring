package com.wrp.mapper;

import com.wrp.domain.Student;

import java.util.List;

public interface StudentMapper {
    public Student findById(int id);

    public List<Student> findAll();

    public void add(Student student);

    public void update(Student student);

    public void delete(int id);
}
