package com.wrp.dao.impl;

import com.wrp.dao.StudentMapper;
import com.wrp.domain.Student;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class StudentMapperImpl implements StudentMapper {

    static SqlSessionFactory builder;
    static {
        InputStream inputStream = null;
        try{
            inputStream = Resources.getResourceAsStream("SqlMapConfig.xml");
            builder = new SqlSessionFactoryBuilder().build(inputStream);
        } catch (IOException e) {
            System.out.println("SqlSession工厂创建失败；");
            e.printStackTrace();
        }
    }


    @Override
    public Student findById(int id) {
        SqlSession sqlSession = builder.openSession(true);
        Student student = sqlSession.selectOne("com.wrp.mapper.StudentMapper.findById", 2);
        sqlSession.close();
        return student;
    }

    @Override
    public List<Student> findAdd() {
        SqlSession sqlSession = builder.openSession(true);
        List<Student> students = sqlSession.selectOne("com.wrp.mapper.StudentMapper.findAll");
        sqlSession.close();
        return students;
    }

    @Override
    public void add(Student student) {
        SqlSession sqlSession = builder.openSession(true);
        sqlSession.insert("com.wrp.mapper.StudentMapper.add", student);
        sqlSession.close();
    }

    @Override
    public void update(Student student) {
        SqlSession sqlSession = builder.openSession(true);
        sqlSession.update("com.wrp.mapper.StudentMapper.update", student);
        sqlSession.close();
    }

    @Override
    public void delete(int id) {
        SqlSession sqlSession = builder.openSession(true);
        sqlSession.delete("com.wrp.mapper.StudentMapper.delete", id);
        sqlSession.close();
    }
}
