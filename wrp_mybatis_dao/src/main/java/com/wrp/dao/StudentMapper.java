package com.wrp.dao;

import com.wrp.domain.Student;

import java.util.List;

public interface StudentMapper {

    public Student findById(int id);

    List<Student> findAdd();

    void add(Student student);

    void update(Student student);

    void delete(int id);
}
