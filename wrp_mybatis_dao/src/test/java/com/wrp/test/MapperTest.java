package com.wrp.test;

import com.wrp.mapper.StudentMapper;
import com.wrp.domain.Student;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;
import org.junit.Test;

import java.io.IOException;
import java.io.InputStream;

public class MapperTest {

    @Test
    public void addTest() throws IOException {
        InputStream inputStream = Resources.getResourceAsStream("SqlMapConfig.xml");
        SqlSessionFactory builder = new SqlSessionFactoryBuilder().build(inputStream);
        SqlSession sqlSession = builder.openSession(true);
        StudentMapper mapper = sqlSession.getMapper(StudentMapper.class);

        Student stu = new Student();
        stu.setName("nic");
        stu.setAge(22);

        mapper.add(stu);
    }
}
