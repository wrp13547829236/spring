package com.wrp.test;

import com.wrp.dao.StudentMapper;
import com.wrp.dao.impl.StudentMapperImpl;
import com.wrp.domain.Student;
import org.junit.Test;

public class DaoTest {

    @Test
    public void findAllTest(){
        StudentMapper sm = new StudentMapperImpl();
        Student stu = sm.findById(2);
        System.out.println(stu);
    }
}
