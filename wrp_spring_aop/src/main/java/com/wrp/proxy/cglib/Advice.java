package com.wrp.proxy.cglib;

public class Advice {

    public void before(){
        System.out.println("前置通知。。。。。");
    }

    public void afterReturning(){
        System.out.println("后置通知。。。。。");
    }
}
