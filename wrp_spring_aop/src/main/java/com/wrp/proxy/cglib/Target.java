package com.wrp.proxy.cglib;

import com.wrp.proxy.jdk.TargetInterface;

public class Target implements TargetInterface {
    public void save() {
        System.out.println("save.....");
    }
}
