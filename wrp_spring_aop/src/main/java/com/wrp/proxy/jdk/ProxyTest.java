package com.wrp.proxy.jdk;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

public class ProxyTest {
    public static void main(String[] args) {

        final TargetInterface target = new Target();
        final Advice advice = new Advice();
        /**
         * 三个参数：
         * ClassLoader loader：目标对象的类加载器
         * Class<?>[] interfaces：目标对象实现的所有接口的数组
         * reflect.InvocationHandler：
         */
        TargetInterface proxy = (TargetInterface) Proxy.newProxyInstance(target.getClass().getClassLoader(),
                target.getClass().getInterfaces(), new InvocationHandler() {
                    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
                        advice.before();
                        Object invoke = method.invoke(target, args);
                        advice.afterReturning();
                        return invoke;
                    }
                });

        proxy.save();
        System.out.println(proxy);
    }
}
