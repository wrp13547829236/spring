package com.wrp.anno;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Component("myAspect")
@Aspect// 表示当前类是一个切面类
public class MyAspect {

    @Before("execution(* com.wrp.anno.*.*(..))")
    public void before(){
        System.out.println("前置通知....");
    }
    // 调用切点
    @AfterReturning("MyAspect.myPoint()")
    public void afterReturning(){
        System.out.println("后置通知....");
    }
    public void Around(ProceedingJoinPoint pjp) {

        try {
            System.out.println("前置环绕通知....");
            Object proceed = pjp.proceed();// 目标对象执行方法
            System.out.println("后置环绕通知....");
        } catch (Throwable throwable) {
            System.out.println("异常环绕通知....");
            throwable.printStackTrace();
        } finally {
            System.out.println("最终环绕通知....");
        }


    }
    public void afterThrowing(){
        System.out.println("异常通知....");
    }
    public void after(){
        System.out.println("最终通知....");
    }

    // 抽取切点表达式
    @Pointcut("execution(* com.wrp.anno.*.*(..))")
    public void myPoint(){}
}
