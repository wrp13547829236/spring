package com.wrp.aop;

import org.aspectj.lang.ProceedingJoinPoint;

public class MyAspect {

    public void before(){
        System.out.println("前置通知....");
    }
    public void afterReturning(){
        System.out.println("后置通知....");
    }
    public void Around(ProceedingJoinPoint pjp) {

        try {
            System.out.println("前置环绕通知....");
            Object proceed = pjp.proceed();// 目标对象执行方法
            System.out.println("后置环绕通知....");
        } catch (Throwable throwable) {
            System.out.println("异常环绕通知....");
            throwable.printStackTrace();
        } finally {
            System.out.println("最终环绕通知....");
        }


    }
    public void afterThrowing(){
        System.out.println("异常通知....");
    }
    public void after(){
        System.out.println("最终通知....");
    }
}
