package com.wrp.interceptor;

import com.wrp.domain.User;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class UserLoginHandlerInterceptor implements HandlerInterceptor {
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        // 判断session中是否有user
        HttpSession session = request.getSession();
        User user = (User) session.getAttribute("user");
        if(user == null){
            // 没有登录
            response.sendRedirect(request.getContextPath() + "/jsp/login.jsp");
            return false;
        }

        return true;
    }
}
