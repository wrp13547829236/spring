package com.wrp.controller;

import com.wrp.domain.Role;
import com.wrp.domain.User;
import com.wrp.service.RoleService;
import com.wrp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;
    @Autowired
    private RoleService roleService;

    @RequestMapping("/addUI")
    public ModelAndView addUI(){
        ModelAndView mv = new ModelAndView();
        List<Role> roleList = roleService.findAll();
        mv.addObject("roleList", roleList);
        mv.setViewName("user-add");
        return mv;
    }

    @RequestMapping("/add")
    public String add(User user, int[] roleIds){
        // 创建User表数据
        int userid = userService.add(user);
        // 创建关系表数据
        userService.addRel(userid, roleIds);
        return "redirect:/user/list";
    }

    @RequestMapping("/list")
    public ModelAndView list(){
        ModelAndView mv = new ModelAndView();
        List<User> userList = userService.findAll();
        mv.addObject("userList", userList);
        mv.setViewName("user-list");
        return mv;
    }

    @RequestMapping("/delete/{userid}")
    // restful风格的编码，需要用@PathVariable注解进行占位符的解析
    public String delete(@PathVariable("userid") int userid){
        userService.delete(userid);
        return "redirect:/user/list";
    }

    @RequestMapping("/login")
    public String login(User user, HttpSession session){
        User user1 = userService.login(user.getUsername(), user.getPassword());
        if(user1 != null){
            // 登录成功，将user存储到session中
            session.setAttribute("user", user);
            return "redirect:/jsp/main.jsp";
        }else{
            return "redirect:/jsp/login.jsp";
        }
    }
}
