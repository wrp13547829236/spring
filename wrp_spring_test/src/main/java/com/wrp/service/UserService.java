package com.wrp.service;

import com.wrp.domain.User;

import java.util.List;

public interface UserService {
    List<User> findAll();

    int add(User user);

    void addRel(int userid, int[] roleIds);

    void delete(int userid);

    User login(String username, String password);
}
