package com.wrp.service;

import com.wrp.domain.Role;

import java.util.List;

public interface RoleService {

    List<Role> findAll();

    void add(Role role);
}
