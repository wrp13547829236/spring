package com.wrp.service.impl;

import com.wrp.dao.RoleDao;
import com.wrp.dao.UserDao;
import com.wrp.domain.Role;
import com.wrp.domain.User;
import com.wrp.service.UserService;

import java.util.List;

public class UserServiceImpl implements UserService {

    private UserDao userDao;
    private RoleDao roleDao;

    public void setUserDao(UserDao userDao) {
        this.userDao = userDao;
    }
    public void setRoleDao(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    @Override
    public List<User> findAll() {
        List<User> userList = userDao.findAll();
        // 有问题：这里的roleList为空
        for( User user: userList){
            int id = user.getUserid();
            List<Role> roles = roleDao.findRoleByUserId(id);
            user.setRoleList(roles);
        }
        return userList;
    }

    @Override
    public int add(User user) {
        return userDao.add(user);
    }

    @Override
    public void addRel(int userid, int[] roleIds) {
        userDao.addRel(userid, roleIds);
    }

    @Override
    public void delete(int userid) {
        // 删除关系表
        userDao.deleteRel(userid);
        // 删除用户表
        userDao.delete(userid);
    }

    @Override
    public User login(String username, String password) {
        User user = userDao.findUserByNameAndPwd(username, password);
        return user;
    }
}
