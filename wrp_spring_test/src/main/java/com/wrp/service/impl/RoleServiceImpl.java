package com.wrp.service.impl;

import com.wrp.dao.RoleDao;
import com.wrp.domain.Role;
import com.wrp.service.RoleService;

import java.util.List;

public class RoleServiceImpl implements RoleService {

    private RoleDao roleDao;
    public void setRoleDao(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    @Override
    public List<Role> findAll() {
        List<Role> roleList = roleDao.findAll();
        return roleList;
    }

    @Override
    public void add(Role role) {
        roleDao.add(role);
    }
}
