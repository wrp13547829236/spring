package com.wrp.dao.impl;

import com.wrp.dao.UserDao;
import com.wrp.domain.User;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class UserDaoImpl implements UserDao {

    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<User> findAll() {
        List<User> userList = jdbcTemplate.query("select * from users", new BeanPropertyRowMapper<User>(User.class));
        return userList;
    }
    // 改造，希望返回添加用户的id
    @Override
    public int add(User user) {
//        jdbcTemplate.update("insert into users values(null, ?, ?)", user.getUsername(), user.getPassword());

        PreparedStatementCreator preparedStatementCreator = new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement preparedStatement = connection.prepareStatement("insert into users values(null, ?, ?)", PreparedStatement.RETURN_GENERATED_KEYS);
                preparedStatement.setString(1,user.getUsername());
                preparedStatement.setString(2,user.getPassword());
                return preparedStatement;
            }
        };
        // 创建keyHolder
        GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(preparedStatementCreator,generatedKeyHolder);

        // 获取生成的主键
        int userid = generatedKeyHolder.getKey().intValue();
        return userid;
    }

    @Override
    public void addRel(int userid, int[] roleIds) {
        for (int id: roleIds){
            jdbcTemplate.update("insert into user_role_rel values(null, ?, ?)", id, userid);
        }
    }

    @Override
    public void deleteRel(int userid) {
        jdbcTemplate.update("delete from user_role_rel where userid = ?", userid);
    }

    @Override
    public void delete(int userid) {
        jdbcTemplate.update("delete from users where userid = ?", userid);
    }

    @Override
    //Spring中使用JdbcTemplate的queryForObject方法，当查不到数据时会抛出如下异常：
    public User findUserByNameAndPwd(String username, String password) {
        User user = null;
        try{
            user = jdbcTemplate.queryForObject("select * from users where username = ? and password = ?", new BeanPropertyRowMapper<User>(User.class),
                    username, password);
        }catch (EmptyResultDataAccessException e){
            return null;
        }
        return user;
    }
}
