package com.wrp.dao.impl;

import com.wrp.dao.RoleDao;
import com.wrp.domain.Role;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class RoleDaoImpl implements RoleDao {

    private JdbcTemplate jdbcTemplate;
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Role> findAll() {
        List<Role> roleList = jdbcTemplate.query("select * from roles", new BeanPropertyRowMapper<Role>(Role.class));
        return roleList;
    }

    @Override
    public void add(Role role) {
        jdbcTemplate.update("insert into roles values(null, ?, ?)", role.getRolename(), role.getRoledesc());
    }

    @Override
    public List<Role> findRoleByUserId(int id) {
        List<Role> roleList = jdbcTemplate.query("select * from user_role_rel ur, roles r where ur.userid = ? and ur.roleid = r.roleid", new BeanPropertyRowMapper<Role>(Role.class), id);
        return roleList;
    }
}
