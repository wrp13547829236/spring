package com.wrp.dao;

import com.wrp.domain.User;

import java.util.List;

public interface UserDao {
    List<User> findAll();

    int add(User user);

    void addRel(int userid, int[] roleIds);

    void deleteRel(int userid);

    void delete(int userid);

    User findUserByNameAndPwd(String username, String password);
}
