package com.wrp.dao;

import com.wrp.domain.Role;

import java.util.List;

public interface RoleDao {
    List<Role> findAll();

    void add(Role role);

    List<Role> findRoleByUserId(int id);
}
