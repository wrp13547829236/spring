<%--
  Created by IntelliJ IDEA.
  User: wrp
  Date: 2021/1/25
  Time: 21:37
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>角色添加</title>
</head>
<body>
<form action="/user/add" method="post">
    用户名称：<input type="text" name="username"/><br/>
    用户密码：<input type="text" name="password" /><br/>
    <c:forEach items="${roleList}" var="role">
        <input type="checkbox" value="${role.roleid}" name="roleIds">${role.rolename}&nbsp;&nbsp;
    </c:forEach>
    <br/><input type="submit" value="添加">
</form>
</body>
</html>
