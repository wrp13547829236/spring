<%--
  Created by IntelliJ IDEA.
  User: wrp
  Date: 2021/1/25
  Time: 18:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>用户列表</title>
    <script>
        function deleteUser(userid){
            if(confirm("你确认要删除该用户吗？")){
                // 状态码 302 临时性重定向
                location.href = "${pageContext.request.contextPath}/user/delete/" + userid;
            }
        }
    </script>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th>用户id</th>
                <th>用户名称</th>
                <th>用户密码</th>
                <th>用户的角色</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${userList}" var="user">
                <tr>
                    <td>${user.userid}</td>
                    <td>${user.username}</td>
                    <td>${user.password}</td>
                    <td>
                        <c:forEach items="${user.roleList}" var="role">
                            ${role.rolename}&nbsp;&nbsp;
                        </c:forEach>
                    </td>
                    <td><a href="javascript:void(0)" onclick="deleteUser('${user.userid}')">删除</a></td>
                </tr>
            </c:forEach>
            <div><a href="/user/addUI">新建</a></div>
        </tbody>
    </table>
</body>
</html>
