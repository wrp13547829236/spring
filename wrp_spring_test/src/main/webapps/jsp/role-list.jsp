<%--
  Created by IntelliJ IDEA.
  User: wrp
  Date: 2021/1/25
  Time: 18:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>角色列表</title>
</head>
<body>
    <table>
        <thead>
            <tr>
                <th>角色id</th>
                <th>角色名称</th>
                <th>角色描述</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${roleList}" var="role">
                <tr>
                    <td>${role.roleid}</td>
                    <td>${role.rolename}</td>
                    <td>${role.roledesc}</td>
                </tr>
            </c:forEach>
            <div><a href="/jsp/role-add.jsp">新建</a></div>
        </tbody>
    </table>
</body>
</html>
