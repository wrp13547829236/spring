package com.wrp.controller;

import com.wrp.service.DemoService;
import com.wrp.service.impl.DemoServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class DemoController {

    @Autowired
    private DemoService demoService;

    @RequestMapping("/demo")
    @ResponseBody
    public void demo(){
        demoService.show1();
    }

    @RequestMapping("/demo1")
    @ResponseBody
    public void demo1(){
        demoService.show2();
    }

    @RequestMapping("/demo2")
    @ResponseBody
    public void dem2(){
        demoService.show4();
    }
}
