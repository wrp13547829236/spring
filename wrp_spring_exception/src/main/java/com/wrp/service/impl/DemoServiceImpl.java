package com.wrp.service.impl;

import com.wrp.exception.MyException;
import com.wrp.service.DemoService;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

@Service("demoService")
public class DemoServiceImpl implements DemoService {
    @Override
    public void show1() {
        System.out.println("类型转换异常");
        Object str = "hello";
        int i = (int)str;
    }

    @Override
    public void show2() {
        System.out.println("除0异常");
        int i = 1/0;
    }

    @Override
    public void show3() throws FileNotFoundException {
        System.out.println("文件找不到异常");
        InputStream is = new FileInputStream("C:/xxx/xxx.txt");
    }

    @Override
    public void show4() {
        System.out.println("控制针异常");
        String str = null;
        str.length();
    }

    @Override
    public void show5() throws MyException {
        System.out.println("自定义异常");
        throw new MyException();
    }
}
