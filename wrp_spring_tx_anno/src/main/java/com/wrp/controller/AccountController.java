package com.wrp.controller;

import com.wrp.domain.Account;
import com.wrp.service.AccountService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class AccountController {
    public static void main(String[] args) {
        ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
        AccountService as = ac.getBean(AccountService.class);
        Account resource = as.queryByName("zs");
        Account target = as.queryByName("tom");

        as.transfer(resource, target, 1000);
    }
}
