package com.wrp.service.impl;

import com.wrp.dao.AccountDao;
import com.wrp.domain.Account;
import com.wrp.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Service("accountService")
@Transactional(timeout = -1)
public class AccountServiceImpl implements AccountService {

    @Autowired
    private AccountDao accountDao;

    @Override
    @Transactional( isolation = Isolation.READ_COMMITTED, propagation = Propagation.REQUIRED)
    public void transfer(Account account_resource, Account account_target, float money) {
        accountDao.addMoney(account_target, money);
        int i = 1 / 0;
        accountDao.reduce(account_resource, money);
    }

    @Override
    public Account queryByName(String name) {
        return accountDao.queryByName(name);
    }
}
