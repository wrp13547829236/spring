package com.wrp.dao.impl;

import com.wrp.dao.AccountDao;
import com.wrp.domain.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class AccountDaoImpl implements AccountDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Override
    public void addMoney(Account account, float money) {
        float newMoney = account.getMoney() + money;
        jdbcTemplate.update("update accounts set money= ? where name= ?",newMoney, account.getName());
    }

    @Override
    public void reduce(Account account, float money) {
        float newMoney = 0;
        if(account.getMoney() < money){
            System.out.println("余额不足！");
        }else{
            newMoney = account.getMoney() - money;
            jdbcTemplate.update("update accounts set money= ? where name= ?",newMoney, account.getName());
        }

    }

    @Override
    public Account queryByName(String name) {
        Account query = jdbcTemplate.queryForObject("select * from accounts where name = ?", new BeanPropertyRowMapper<Account>(Account.class), name);
        return query;
    }

}
