package com.wrp.dao;

import com.wrp.domain.Account;

public interface AccountDao {

    /**
     * 加钱
     * @param account
     * @param money
     */
    public void addMoney(Account account, float money);

    /**
     * 减钱
     * @param account
     * @param money
     */
    public void reduce(Account account, float money);

    public Account queryByName(String name);

}
