import com.wrp.domain.Account;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class JdbcTemplateCRUDTest {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Test
    public void update(){
        jdbcTemplate.update("update accounts set money = ? where name = ?", 9000, "zs");
    }

    @Test
    public void delete(){
        jdbcTemplate.update("delete from accounts where name = ?", "ll");
    }

    @Test
    public void insert(){
        jdbcTemplate.update("insert into accounts values(?,?,?)", null, "tom", 10000);
    }

    @Test
    public void queryAll(){
        List<Account> list = jdbcTemplate.query("select * from accounts", new BeanPropertyRowMapper<Account>(Account.class));

        for (Account account:
             list) {
            System.out.println(account);
        }
    }

    @Test
    public void queryOne(){
        Account account = jdbcTemplate.queryForObject("select * from Accounts where name = ?", new BeanPropertyRowMapper<Account>(Account.class), "tom");
        System.out.println(account);
    }

    @Test
    public void queryCount(){
        Integer count = jdbcTemplate.queryForObject("select count(1) from accounts", int.class);
        System.out.println(count);
    }
}
