import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

public class JdbcTemplateTest {

    @Autowired
    private DataSource dataSource;

    @Test
    public void insert(){
        //创建jdbcTemplate
        JdbcTemplate jt = new JdbcTemplate();
        // 设置数据源
        jt.setDataSource(dataSource);
        // 操作
        int count = jt.update("insert into accounts values(?, ?, ?)", null, "zs", "10000");
        System.out.println(count);
    }
}
