package com.wrp.test;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

public class JdbcTemplateTest {

    @Test
    public void insert(){
        ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");
        DataSource dataSource = (DataSource) ac.getBean("dataSource");

        //创建jdbcTemplate
        JdbcTemplate jt = new JdbcTemplate();// 无参构造
        // 设置数据源
        jt.setDataSource(dataSource);// set方法设置数据源
        // 操作
        int count = jt.update("insert into accounts values(?, ?, ?)", null, "ww", "10000");
        System.out.println(count);
    }

    @Test
    public void insert1(){
        ApplicationContext ac = new ClassPathXmlApplicationContext("applicationContext.xml");

        //创建jdbcTemplate
        JdbcTemplate jt = (JdbcTemplate) ac.getBean("jdbcTemplate");
        // 操作
        int count = jt.update("insert into accounts values(?, ?, ?)", null, "ll", "10000");
        System.out.println(count);
    }
}
